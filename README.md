
# TOTTEM

---

## BASE DE DATOS - FUNDACION JALA

### INTEGRANTES - GRUPO 3 :

- Manuel Morales
- Gabriela Trujillo
- Hugo Oropeza
- Jose Teran

## INSTRUCCIONES PARA LEVANTAR LA BASE DE DATOS

Dentro del directorio ProjectoDB ejecutar el script build.sh

```
./build.sh
```

> Ingresar al navegador de su preferencia e ingrese al siguiente
> [LINK](https://localhost/).

- Correo: admin@admin.com
- Password: admin

Añada un nuevo servidor y en la pestaña de *Connection*
igrese los siguientes valores.

- Host name: postgres
- Username: root
- Password: root

El script esta diseñado para ingresar las tablas, funciones e inserts 
de la base de datos es por ello que puede tardar unos segundos.

