SELECT * FROM historial_proveedores;

SELECT * FROM proveedor;

SELECT * FROM unidad_compra;

SELECT * FROM historial_unidades_compra;

SELECT * FROM importe;

SELECT * FROM historial_importes;

SELECT * FROM servicio;

SELECT * FROM historial_servicios;

SELECT * FROM vivienda;

SELECT * FROM zona;

SELECT * FROM tipo_vivienda;

SELECT * FROM cliente_servicio;

SELECT * FROM cliente;

SELECT * FROM cliente_empleado;

SELECT * FROM empleado;

SELECT * FROM entrevista;

SELECT * FROM postulante;

SELECT * FROM datos;

SELECT * FROM historial_datos;

SELECT * FROM historial_empleados_rendimientos;

SELECT * FROM empleado_rendimiento;

SELECT * FROM rendimiento;

SELECT * FROM calificaciones;

SELECT * FROM sueldo;

SELECT * FROM historial_sueldos;

SELECT * FROM cargo;

SELECT * FROM departamento;

SELECT importe.cantidad
FROM proveedor, unidad_compra, importe
WHERE importe.id_unidad_compra = unidad_compra.id_unidad_compra
AND unidad_compra.id_proveedor = proveedor.id_proveedor
AND proveedor.id_proveedor IN (1,3);

SELECT cliente.nit
FROM vivienda, cliente, zona
WHERE cliente.id_cliente = vivienda.id_cliente
AND vivienda.id_zona = zona.id_zona
AND zona.id_zona IN (1,2,4);

SELECT cliente.nit
FROM vivienda, cliente, tipo_vivienda
WHERE cliente.id_cliente = vivienda.id_cliente
AND vivienda.id_tipo_vivienda = tipo_vivienda.id_tipo_vivienda
AND tipo_vivienda.tipo IN ('C', 'E');

SELECT cliente.nombre_1
FROM cliente
WHERE cliente.nombre_1 LIKE '%as%';

SELECT datos.nombre2
FROM datos
WHERE datos.nombre2 LIKE 'D%';

SELECT postulante.apellido_2
FROM postulante
WHERE postulante.apellido_2 LIKE '%z';

SELECT cliente.nombre_1, postulante.nombre_1
FROM cliente
INNER JOIN postulante ON  cliente.nombre_1 = postulante.nombre_1;

SELECT departamento.departamento, cargo.cargo
FROM departamento
RIGHT JOIN cargo ON  cargo.id_departamento = departamento.id_departamento;

SELECT cargo.cargo, datos.nombre1
FROM datos, empleado
LEFT JOIN cargo ON  cargo.id_cargo = empleado.id_cargo 
WHERE datos.id_empleado = empleado.id_empleado;

SELECT cliente.telefono
FROM cliente, cliente_servicio
WHERE cliente.id_cliente = cliente_Servicio.id_cliente
GROUP BY cliente.telefono;

SELECT datos.correo_electronico, sueldo.sueldo
FROM datos, empleado, sueldo
WHERE empleado.id_empleado = sueldo.id_empleado
AND datos.id_empleado = empleado.id_empleado
GROUP BY sueldo.sueldo, datos.correo_electronico;

SELECT empleado.id_empleado, rendimiento.rendimiento
FROM empleado, rendimiento, empleado_rendimiento
WHERE empleado.id_empleado = empleado_rendimiento.id_empleado
AND rendimiento.id_rendimiento = empleado_rendimiento.id_rendimiento
GROUP BY empleado.id_empleado, rendimiento.id_rendimiento;

SELECT datos.estado_civil, datos.nombre1
FROM datos, empleado
WHERE empleado.id_empleado = datos.id_empleado
AND empleado.id_empleado IN (1,21,35,24,5,7,8,9,10,11,12,13,14,15,16,17,20,31)
AND (datos.nombre1 Like 'A%'
OR datos.nombre1 Like 'L%');

SELECT COUNT(*)
FROM datos
where datos.telefono LIKE '%65';

SELECT COUNT(*)
FROM cliente;

