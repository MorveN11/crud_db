#!/bin/bash
# -*- ENCODING: UTF-8 -*-

# shellcheck disable=SC2164
mkdir venv
python -m venv venv

cd src/

docker-compose up -d

echo 'La base de datos Se ha iniciado con Exito!'

cd ../

source venv/bin/activate
 
pip install --upgrade pip
pip install psycopg2-binary
pip install psycopg2

venv/bin/python -m src.main &
